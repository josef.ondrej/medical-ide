Medical IDE
===========

<b>Nástroj pro doktory na psaní a pro pacienty na prohlížení lékařských zpráv. </b> 

<img src="./img/medical_IDE_templates.png">
<br>
<br>
<img src="./img/medical_IDE_basic.png">

Základní funkcionalita bude prostě textový editor s nějakým základním formátováním a nad tím můžeme stavět další 
funkce jako například: 

Basic: 
- Podpora templatů pro různé zprávy, možnost vytvářet si vlastní templaty.
- Autocompletion a tooltipy pro lékařské termíny.
- Podpora pro diferenciální diagnózu a různé standardní postupy - teď si doktor často musí pamatovat x otázek a 
podle toho stanovit diagnózu, pak to píše jak pohádku, přitom je to jasné vyplňění formuláře. 
 
More advanced: 
- Automatická oprava errorů, warningy pokud například lékař předepíše pacientovi nějaký lék, na který 
má kontraindikaci podle dostupných informací. 
- Doplňování nejpravděpodobnější diagnózy, vhodné léčby. 
- Automatické generování dalších dokumentů, jako například předpis léku.  
- OCR lékařské zprávy z fotky do našeho formátu  
- Propojení se staršími zprávami od pacienta, tooltipy co zohledňují předchozí diagnózy, 
automatické notifikace pacientovi, když má přijít na kontrolu - prostě taková online zdravotní karta. 
Tady asi člověk nechce sdílet všechny informace se všema doktorama, takže by bylo třeba nastavit nějaké přístupové 
práva. 

Provedení:
- Asi bych chtěl mít nějaký vlastní MarkDown-like formát, ve kterém by to ten editor zapisoval. 
Obecně by byl editor použitelný na hodně věcí, naše přidaná hodnota by byla v tom, že bysme uměli 
vytvořit přesně <b>template</b>, jaký kdo potřebuje a <b>přidávat pluginy</b> 
typu: Diferenciální diagnóza mozkové mrtvice.   

- V počáteční fázi se ty data nemusí nikde ukládat na našem serveru, stačí když to bude fakt jenom editor/prohlížeč, 
takže nám odpadnou nějaké security issues. 